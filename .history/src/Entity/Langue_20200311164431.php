<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangueRepository")
 */
class Langue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libeller;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niveau;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cv", inversedBy="parler")
     */
    private $langue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibeller(): ?string
    {
        return $this->libeller;
    }

    public function setLibeller(string $libeller): self
    {
        $this->libeller = $libeller;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getLangue(): ?Cv
    {
        return $this->langue;
    }

    public function setLangue(?Cv $langue): self
    {
        $this->langue = $langue;

        return $this;
    }
}
