<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MetierRepository")
 */
class Metier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Secteur", inversedBy="secteur")
     */
    private $secteur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Cv", mappedBy="metier")
     */
    private $cvs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offre", inversedBy="metiers")
     */
    private $offr;

    public function __construct()
    {
        $this->cvs = new ArrayCollection();
        $this->offr = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSecteur(): ?Secteur
    {
        return $this->secteur;
    }

    public function setSecteur(?Secteur $secteur): self
    {
        $this->secteur = $secteur;

        return $this;
    }

    /**
     * @return Collection|Cv[]
     */
    public function getCvs(): Collection
    {
        return $this->cvs;
    }

    public function addCv(Cv $cv): self
    {
        if (!$this->cvs->contains($cv)) {
            $this->cvs[] = $cv;
            $cv->addMetier($this);
        }

        return $this;
    }

    public function removeCv(Cv $cv): self
    {
        if ($this->cvs->contains($cv)) {
            $this->cvs->removeElement($cv);
            $cv->removeMetier($this);
        }

        return $this;
    }

    /**
     * @return Collection|Offre[]
     */
    public function getOffr(): Collection
    {
        return $this->offr;
    }

    public function addOffr(Offre $offr): self
    {
        if (!$this->offr->contains($offr)) {
            $this->offr[] = $offr;
        }

        return $this;
    }

    public function removeOffr(Offre $offr): self
    {
        if ($this->offr->contains($offr)) {
            $this->offr->removeElement($offr);
        }

        return $this;
    }
}
