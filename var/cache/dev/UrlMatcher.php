<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/api/inscriptionannonceur' => [[['_route' => 'inscriptionannonceur', '_controller' => 'App\\Controller\\AnnonceurController::addannonceur'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/lister/users' => [[['_route' => 'user_entreprise', '_controller' => 'App\\Controller\\AnnonceurController::ListerAnnonceur'], null, ['GET' => 0], null, false, false, null]],
        '/api/inscriptionC' => [[['_route' => 'inscriptionC', '_controller' => 'App\\Controller\\CandidatController::inscriptionCandidat'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/lister/candidats' => [[['_route' => 'candidat_user', '_controller' => 'App\\Controller\\CandidatController::show'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/metier' => [[['_route' => 'metier', '_controller' => 'App\\Controller\\CandidatController::ListerMetier'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/secteur' => [[['_route' => 'secteur', '_controller' => 'App\\Controller\\CandidatController::ListerSecteur'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/rubrique' => [[['_route' => 'rubrique', '_controller' => 'App\\Controller\\CandidatController::ListerRubrique'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/annonce' => [[['_route' => 'annonce', '_controller' => 'App\\Controller\\OffreController::faireannonce'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/parannonceur' => [[['_route' => 'parannonceur', '_controller' => 'App\\Controller\\OffreController::listeposter'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/lister/annonces' => [[['_route' => 'offreannonceur', '_controller' => 'App\\Controller\\OffreController::ListerOffre'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/parcandidat' => [[['_route' => 'listecandidat', '_controller' => 'App\\Controller\\OffreController::listpostule'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/cv' => [[['_route' => 'cv', '_controller' => 'App\\Controller\\OffreController::ajoutcv'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/experience' => [[['_route' => 'experience', '_controller' => 'App\\Controller\\OffreController::ajoutexperience'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/formation' => [[['_route' => 'formation', '_controller' => 'App\\Controller\\OffreController::ajoutformation'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/video' => [[['_route' => 'video', '_controller' => 'App\\Controller\\OffreController::ajoutvideo'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/langue' => [[['_route' => 'langue', '_controller' => 'App\\Controller\\OffreController::ajoutlangue'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/listecv' => [[['_route' => 'listecv', '_controller' => 'App\\Controller\\OffreController::listecv'], null, ['GET' => 0], null, false, false, null]],
        '/api/security/postuler' => [[['_route' => 'postuler', '_controller' => 'App\\Controller\\OffreController::postuler'], null, ['POST' => 0], null, false, false, null]],
        '/api/security/listelangue' => [[['_route' => 'listelangue', '_controller' => 'App\\Controller\\OffreController::listelangue'], null, ['GET' => 0], null, false, false, null]],
        '/api/logincheck' => [[['_route' => 'login', '_controller' => 'App\\Controller\\SecurityController::login'], null, ['POST' => 0, 'GET' => 1], null, false, false, null]],
        '/api/resetting' => [[['_route' => 'resetting', '_controller' => 'App\\Controller\\SecurityController::resetPassword'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/api(?'
                    .'|/(?'
                        .'|security/(?'
                            .'|annonceur/bloquer/([^/]++)(*:218)'
                            .'|candidat/bloquer/([^/]++)(*:251)'
                        .')'
                        .'|annonceur/update/([^/]++)(*:285)'
                        .'|entreprise/update/([^/]++)(*:319)'
                        .'|candidat/update/([^/]++)(*:351)'
                    .')'
                    .'|(?:/(index)(?:\\.([^/]++))?)?(*:388)'
                    .'|/(?'
                        .'|docs(?:\\.([^/]++))?(*:419)'
                        .'|contexts/(.+)(?:\\.([^/]++))?(*:455)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        218 => [[['_route' => 'bloquer_debloquer_annonceur', '_controller' => 'App\\Controller\\AnnonceurController::bloqueAnnonceur'], ['id'], ['GET' => 0], null, false, true, null]],
        251 => [[['_route' => 'bloquer_debloquer_candidat', '_controller' => 'App\\Controller\\CandidatController::bloquerCandidat'], ['id'], ['GET' => 0], null, false, true, null]],
        285 => [[['_route' => 'update_annonceur', '_controller' => 'App\\Controller\\AnnonceurController::updateAnnonceur'], ['id'], ['POST' => 0], null, false, true, null]],
        319 => [[['_route' => 'update_entreprise', '_controller' => 'App\\Controller\\AnnonceurController::updateEntreprise'], ['id'], ['POST' => 0], null, false, true, null]],
        351 => [[['_route' => 'update_candidat', '_controller' => 'App\\Controller\\CandidatController::updateCandidat'], ['id'], ['POST' => 0], null, false, true, null]],
        388 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        419 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        455 => [
            [['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
