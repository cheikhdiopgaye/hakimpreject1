<?php

namespace App\Repository;

use App\Entity\Useroffre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Useroffre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Useroffre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Useroffre[]    findAll()
 * @method Useroffre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UseroffreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Useroffre::class);
    }

    // /**
    //  * @return Useroffre[] Returns an array of Useroffre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Useroffre
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
