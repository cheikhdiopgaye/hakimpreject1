<?php

namespace App\Repository;

use App\Entity\Cvlangue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cvlangue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cvlangue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cvlangue[]    findAll()
 * @method Cvlangue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CvlangueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cvlangue::class);
    }

    // /**
    //  * @return Cvlangue[] Returns an array of Cvlangue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cvlangue
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
