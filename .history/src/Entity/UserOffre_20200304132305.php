<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserOffreRepository")
 */
class UserOffre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userOffres")
     */
    private $UserOffre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserOffre(): ?User
    {
        return $this->UserOffre;
    }

    public function setUserOffre(?User $UserOffre): self
    {
        $this->UserOffre = $UserOffre;

        return $this;
    }
}
