<?php

namespace App\Repository;

use App\Entity\Cvformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cvformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cvformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cvformation[]    findAll()
 * @method Cvformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CvformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cvformation::class);
    }

    // /**
    //  * @return Cvformation[] Returns an array of Cvformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cvformation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
