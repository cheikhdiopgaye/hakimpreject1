<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MetierRepository")
 */
class Metier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offreuser"})
     */
    private $libelle;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Secteur", inversedBy="metiers")
     * @Groups({"offreuser"})
     */
    private $metiersecteur;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offre", mappedBy="offremetier")
     * @Groups({"offreuser"})
     */
    private $offres;

    public function __construct()
    {
        $this->offres = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getMetiersecteur(): ?Secteur
    {
        return $this->metiersecteur;
    }

    public function setMetiersecteur(?Secteur $metiersecteur): self
    {
        $this->metiersecteur = $metiersecteur;

        return $this;
    }

    /**
     * @return Collection|Offre[]
     */
    public function getOffres(): Collection
    {
        return $this->offres;
    }

    public function addOffre(Offre $offre): self
    {
        if (!$this->offres->contains($offre)) {
            $this->offres[] = $offre;
            $offre->setOffremetier($this);
        }

        return $this;
    }

    public function removeOffre(Offre $offre): self
    {
        if ($this->offres->contains($offre)) {
            $this->offres->removeElement($offre);
            // set the owning side to null (unless already changed)
            if ($offre->getOffremetier() === $this) {
                $offre->setOffremetier(null);
            }
        }

        return $this;
    }

}
