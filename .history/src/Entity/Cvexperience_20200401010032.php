<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CvexperienceRepository")
 */
class Cvexperience
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"offreuser"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\cv", inversedBy="cvexperiences")
     * @Groups({"offreuser"})
     */
    private $cv;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Experience", inversedBy="cvexperiences")
     * @Groups({"offreuser"})
     */
    private $experience;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCv(): ?cv
    {
        return $this->cv;
    }

    public function setCv(?cv $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }
}
