<?php

namespace App\Repository;

use App\Entity\Cvexperience;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cvexperience|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cvexperience|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cvexperience[]    findAll()
 * @method Cvexperience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CvexperienceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cvexperience::class);
    }

    // /**
    //  * @return Cvexperience[] Returns an array of Cvexperience objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cvexperience
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
